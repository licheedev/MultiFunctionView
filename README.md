# MultiFunctionView

```gradle
allprojects {
  repositories {
    ...
    mavenCentral()
  }
}


implementation 'com.licheedev:multifunctionview:1.0.8'
```

属性
```xml
<declare-styleable name="MultiFunctionView">
    <attr name="allTextSize" format="dimension" />
    <attr name="widgetMargin" format="dimension" />

    <attr name="leftLabelText" format="string" />
    <attr name="leftLabelColor" format="color" />
    <attr name="leftLabelTextSize" format="dimension" />
    <attr name="leftLabelWidth" format="dimension" />
    <attr name="leftMarqueeRepeatLimit" format="integer" />


    <attr name="rightLabelText" format="string" />
    <attr name="rightLabelTextSize" format="dimension" />
    <attr name="rightLabelColor" format="color" />

    <attr name="button1Text" format="string" />
    <attr name="button2Text" format="string" />
    <attr name="buttonTextColor" format="color" />
    <attr name="buttonTextSize" format="dimension" />
    <attr name="buttonBg" format="reference" />
    <attr name="showButton">
        <flag name="DEFAULT" value="-1" />
        <flag name="NONE" value="0" />
        <flag name="FIRST" value="1" />
        <flag name="SECOND" value="2" />
        <flag name="All" value="3" />
    </attr>

    <attr name="editHintColor" format="color" />
    <attr name="editHintText" format="string" />
    <attr name="editValueColor" format="color" />
    <attr name="editValueText" format="string" />
    <attr name="editSelectionTextSize" format="dimension" />
    <attr name="editSelectionWidth" format="dimension" />
    <attr name="editSelectionHeight" format="dimension" />
    <attr name="selectionArrowImage" format="reference" />
    <attr name="editTextMaxLength" format="integer" />
    <attr name="editInputMode">
        <flag name="NUMBER" value="1" />
        <flag name="CHARACTER" value="2" />
        <flag name="NEGATIVE" value="3" />
    </attr>
    <attr name="isEditText" format="boolean" />
    <attr name="ifSelectionBg" format="reference" />
    <attr name="ifEditBg" format="reference" />
    <attr name="showSelectionArrow" format="boolean" />
    <attr name="showEditSelection" format="boolean" />
    <attr name="spinnerItemLayout" format="reference" />
    <attr name="spinnerItemTextId" format="reference" />
    
</declare-styleable>

```

BindingAdapter
```kotlin
@BindingAdapter("app:mfvEditValue")
fun mfvEditValue(view: MultiFunctionView, value: Any?) {
    if (value == null) {
        view.editText.setText("", false)
    } else {
        view.editText.setText(value.toString(), false)
    }
}

@BindingAdapter("app:mfvSelectionData")
fun mfvSelectionData(view: MultiFunctionView, map: Map<String, *>?) {
    if (map == null) {
        return
    }
    view.setData(map)
}

@BindingAdapter("app:mfvSelectionData")
fun mfvSelectionData(view: MultiFunctionView, list: List<*>?) {
    if (list == null) {
        return
    }
    view.setData(list.map { it.toString() })
}

@BindingAdapter("app:mfvButton1Action")
fun mfvButton1Action(view: MultiFunctionView, action: (() -> Unit)?) {
    view.button1.setOnClickListener {
        action?.invoke()
    }
}

@BindingAdapter("app:mfvButton2Action")
fun mfvButton2Action(view: MultiFunctionView, action: (() -> Unit)?) {
    view.button2.setOnClickListener {
        action?.invoke()
    }
}


```