package com.licheedev.serialportpathdialogdemo

import androidx.databinding.BindingAdapter
import com.licheedev.multifunctionview.MultiFunctionView

@BindingAdapter("app:mfvEditValue")
fun mfvEditValue(view: MultiFunctionView, value: Any?) {
    if (value == null) {
        view.editText.setText("", false)
    } else {
        view.editText.setText(value.toString(), false)
    }
}

@BindingAdapter("app:mfvSelectionData")
fun mfvSelectionData(view: MultiFunctionView, map: Map<String, *>?) {
    if (map == null) {
        return
    }
    view.setData(map)
}

@BindingAdapter("app:mfvSelectionData")
fun mfvSelectionData(view: MultiFunctionView, list: List<*>?) {
    if (list == null) {
        return
    }
    view.setData(list.map { it.toString() })
}

@BindingAdapter("app:mfvButton1Action")
fun mfvButton1Action(view: MultiFunctionView, action: (() -> Unit)?) {
    view.button1.setOnClickListener {
        action?.invoke()
    }
}

@BindingAdapter("app:mfvButton2Action")
fun mfvButton2Action(view: MultiFunctionView, action: (() -> Unit)?) {
    view.button2.setOnClickListener {
        action?.invoke()
    }
}

