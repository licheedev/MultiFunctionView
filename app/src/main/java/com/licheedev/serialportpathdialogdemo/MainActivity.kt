package com.licheedev.serialportpathdialogdemo

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.licheedev.serialportpathdialogdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    
    private val viewModel by viewModels<MyViewModel>()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.viewModel = viewModel
    }

    class MyViewModel : ViewModel() {
        
        val jidianqi = mapOf(
            "动力电机" to 0x01,
            "谷仓照明" to 0x02,
            "米仓照明" to 0x03,
            "米仓门锁" to 0x04,
            "风扇1" to 0x05,
            "风扇2" to 0x06,
            "风扇3" to 0x07,
            "风扇4" to 0x08,
            "报警" to 0x09,
            "取米按键LED" to 0x0A
        )

    }

}